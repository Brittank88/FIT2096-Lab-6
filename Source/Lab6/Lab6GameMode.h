// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab6GameMode.generated.h"

UCLASS(minimalapi)
class ALab6GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALab6GameMode();
};



