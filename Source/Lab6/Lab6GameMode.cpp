// Copyright Epic Games, Inc. All Rights Reserved.

#include "Lab6GameMode.h"
#include "Lab6HUD.h"
#include "Lab6Character.h"
#include "UObject/ConstructorHelpers.h"

ALab6GameMode::ALab6GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ALab6HUD::StaticClass();
}
